<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!--A Design by W3layouts 
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
	<head>
		<title>Charm | Cadastrar</title>
		<link href="css/bootstrap-3.1.1.min.css" rel='stylesheet' type='text/css' />
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="js/jquery.min.js"></script>
		<!-- Custom Theme files -->
		<!--theme-style-->
		<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
		<!--//theme-style-->
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="Youth Fashion Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
		Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
		<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
		<link href='//fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
		<link href='//fonts.googleapis.com/css?family=Poiret+One' rel='stylesheet' type='text/css'>
		<!-- start menu -->
		<script src="js/bootstrap.min.js"></script>
		<script src="js/simpleCart.min.js"> </script>
		<!-- slide -->
		<script src="js/responsiveslides.min.js"></script>
		<script>
			$(function () {
				$("#slider").responsiveSlides({
					auto: true,
					speed: 500,
					namespace: "callbacks",
					pager: true,
				});
			});
		</script>
	</head>
	<body>
		<!--header-->
		<div class="header">
			<div class="header-top">
				<div class="container">
					<div class="col-sm-4 logo animated wow fadeInLeft" data-wow-delay=".5s">
						<h1><a href="index.html"><img src="images/charm.png" alt="cart"/><span>CHARM</span></a></h1>	
					</div>
					<div class="col-sm-3 world animated wow fadeInRight" data-wow-delay=".5s">
					<div class="cart box_1">
						<a href="checkout.html">
							<h3> 
								<div class="total">
								<span class="simpleCart_total"></span></div>
								<img src="images/cart.png" alt="cart"/>
							</h3>
						</a>
						<p><a href="javascript:;" class="simpleCart_empty">Carrinho Vazio</a></p>
					</div>
					</div>
					<div class="col-sm-3 number animated wow fadeInRight" data-wow-delay=".5s">
						<span><i class="glyphicon glyphicon-envelope"> </i> contato@charm.com.br</span>
						<p>Envie um Email</p>
					</div>
					<div class="col-sm-2 search animated wow fadeInRight" data-wow-delay=".5s">		
						<a class="play-icon popup-with-zoom-anim" href="#small-dialog"><i class="glyphicon glyphicon-search"> </i> </a>
					</div>
					<div class="clearfix"> </div>
				</div>
			</div>
			<div class="container">
				<div class="head-top">
					<div class="n-avigation">
						<nav class="navbar nav_bottom" role="navigation">
							<!-- Brand and toggle get grouped for better mobile display -->
							<div class="navbar-header nav_2">
								<button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
									<span class="sr-only">Toggle navigation</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
								<a class="navbar-brand" href="#"></a>
							</div> 
							<!-- Collect the nav links, forms, and other content for toggling -->
							<div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
								<ul class="nav navbar-nav nav_1">
									<li><a href="femininos.jsp">Femininos</a></li>
									<li><a href="masculino.jsp">Masculinos</a></li>
									<li><a href="produtos.jsp">Todos</a></li>
									<li><a href="login.jsp">Login</a></li>
									<li class="last"><a href="contato.jsp">Fale Conosco</a></li>
								</ul>
							</div>
							<!-- /.navbar-collapse -->
						</nav>
					</div>
					<div class="clearfix"> </div>
					<!---pop-up-box---->   
					<link href="css/popuo-box.css" rel="stylesheet" type="text/css" media="all"/>
					<script src="js/jquery.magnific-popup.js" type="text/javascript"></script>
					<!---//pop-up-box---->
					<div id="small-dialog" class="mfp-hide">
						<div class="search-top">
							<div class="login">
								<form action="#" method="post">
									<input type="submit" value="">
									<input type="text" name="search" value="Digite nome, marca, linha..." onfocus="this.value = '';" onblur="if (this.value == '') {this.value = '';}">		
								</form>
							</div>
							<p>	Shopping</p>
						</div>				
					</div>
					<script>
						$(document).ready(function() {
							$('.popup-with-zoom-anim').magnificPopup({
								type: 'inline',
								fixedContentPos: false,
								fixedBgPos: true,
								overflowY: 'auto',
								closeBtnInside: true,
								preloader: false,
								midClick: true,
								removalDelay: 300,
								mainClass: 'my-mfp-zoom-in'
							});														
						});
					</script>			
					<!---->		
				</div>
			</div>
		</div>
		<!--//header-->
	<div class="breadcrumbs">
			<div class="container">
				<ol class="breadcrumb breadcrumb1 animated wow slideInLeft animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: slideInLeft;">
					<li><a href="index.html"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Home</a></li>
					<li class="active">Cadastro</li>
				</ol>
			</div>
		</div>
	<div class="container">
		<div class="register">
			<h2>Cadastro</h2>
			  	  <form action="#" method="post">
					 <div class="col-md-6  register-top-grid">
						
						<div class="mation">
							<span>Nome(s)</span>
							<input type="text" name="firstname"> 
						
							<span>Sobrenome(s)</span>
							<input type="text" name="lastname"> 
						 
							 <span>Email</span>
							 <input type="text" name="email"> 
						</div>
						 <div class="clearfix"> </div>
						   <a class="news-letter" href="#">
							 <label class="checkbox"><input type="checkbox" name="checkbox" checked=""><i> </i>Sign Up</label>
						   </a>
						 </div>
					     <div class=" col-md-6 register-bottom-grid">
							   
								<div class="mation">
									<span>Senha</span>
									<input type="password" name="password">
									<span>Confirmar Senha</span>
									<input type="password" name="password">
								</div>
						 </div>
						 <div class="clearfix"> </div>
					</form>
					
					<div class="register-but">
					  <form action="#" method="post">
						   <input type="submit" value="submit">
						   <div class="clearfix"> </div>
					   </form>
					</div>
			   </div>
		</div>
	
		<!--footer-->
		<div class="footer">
			<div class="container">
				<div class="footer-top">
					<div class="col-md-12 top-footer animated wow fadeInLeft text-right" data-wow-delay=".5s">
						<h3>Siga-nos</h3>
						<div class="social-icons">
							<ul class="social">
								<li><a href="#"><i></i></a> </li>
								<li><a href="#"><i class="facebook"></i></a></li>	
								<li><a href="#"><i class="google"></i> </a></li>
								<li><a href="#"><i class="linked"></i> </a></li>						
							</ul>
								<div class="clearfix"></div>
						 </div>
					</div>
					<div class="clearfix"> </div>	
				</div>	
			</div>
				<div class="footer-bottom">
				<div class="container">
					<div class="col-md-4 footer-bottom-cate animated wow fadeInLeft" data-wow-delay=".5s">
						<h6>Categorias</h6>
						<ul>
							<li><a href="women.html">Feminino</a></li>
							<li><a href="men.html">Masculino</a></li>
							<li><a href="products.html">Todos</a></li>
						</ul>
					</div>
					<div class="col-md-4 footer-bottom-cate animated wow fadeInRight" data-wow-delay=".5s">
						<h6>Saiba Mais</h6>
						<ul>
							<li><a href="#">Quem Somos</a></li>
							<li><a href="#">Contate-nos</a></li>
							<li><a href="#">FAQ</a></li>
							<li><a href="#">Condi��es de Uso</a></li>
						</ul>
					</div>
					<div class="col-md-4 footer-bottom-cate cate-bottom animated wow fadeInRight" data-wow-delay=".5s">
						<h6>Nosso Endereco</h6>
						<ul>
							<li><i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i> Rua Wonderland n� 5669 - Centro <span>S�o Paulo - SP.</span></li>
							<li><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i>Email : <a href="mailto:info@example.com">suporte@charm.com.br</a></li>
							<li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i>Telefone : +55 (11) 9 9999-9999</li>
						</ul>
					</div>
					<div class="clearfix"> </div>
					<p class="footer-class animated wow fadeInUp animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInUp;"> � 2021 Luck&Charm Eireli. Todos os direitos reservados | Design Desenvolvido Por <a href="http://w3layouts.com/" target="_blank">W3layouts</a> </p>
				</div>
			</div>
		</div>
		<!--footer-->
	</body>
</html>