package www.charm.com.br.dominio;

import java.time.LocalDate;

public class Endereco extends EntidadeDominio {
	
	private String logradouro; 
	private String tipoLogradouro; 
	private String numero; 
	private String bairro; 
	private String tipoResidência; 
	private String cep; 
	private String observacao; 
	private Cidade cidade; 
	private TipoEndereco tipoEndereco;
	
	public Endereco(int id, LocalDate dataCadastro, String logradouro, String tipoLogradouro, String numero,
			String bairro, String tipoResidência, String cep, String observacao, Cidade cidade,
			TipoEndereco tipoEndereco) {
		super(id, dataCadastro);
		this.logradouro = logradouro;
		this.tipoLogradouro = tipoLogradouro;
		this.numero = numero;
		this.bairro = bairro;
		this.tipoResidência = tipoResidência;
		this.cep = cep;
		this.observacao = observacao;
		this.cidade = cidade;
		this.tipoEndereco = tipoEndereco;
	}
	
	public String getLogradouro() {
		return logradouro;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	public String getTipoLogradouro() {
		return tipoLogradouro;
	}
	public void setTipoLogradouro(String tipoLogradouro) {
		this.tipoLogradouro = tipoLogradouro;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public String getTipoResidência() {
		return tipoResidência;
	}
	public void setTipoResidência(String tipoResidência) {
		this.tipoResidência = tipoResidência;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public Cidade getCidade() {
		return cidade;
	}
	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}
	public TipoEndereco getTipoEndereco() {
		return tipoEndereco;
	}
	public void setTipoEndereco(TipoEndereco tipoEndereco) {
		this.tipoEndereco = tipoEndereco;
	} 
}
