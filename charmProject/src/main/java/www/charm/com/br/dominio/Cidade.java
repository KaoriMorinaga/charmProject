package www.charm.com.br.dominio;

import java.time.LocalDate;

public class Cidade extends EntidadeDominio {
	
	private String nome; 
	private Estado estado;
	
	public Cidade(int id, LocalDate dataCadastro, String nome, Estado estado) {
		super(id, dataCadastro);
		this.nome = nome;
		this.estado = estado;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Estado getEstado() {
		return estado;
	}
	public void setEstado(Estado estado) {
		this.estado = estado;
	}
}
