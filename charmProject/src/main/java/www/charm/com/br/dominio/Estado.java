package www.charm.com.br.dominio;

import java.time.LocalDate;

public class Estado extends EntidadeDominio{
	
	private String nome; 
	private String sigla;
	
	public Estado(int id, LocalDate dataCadastro, String nome, String sigla) {
		super(id, dataCadastro);
		this.nome = nome;
		this.sigla = sigla;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getSigla() {
		return sigla;
	}
	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
}
