package www.charm.com.br.dominio;

import java.time.LocalDate;

public class BandeiraCartaoCredito extends EntidadeDominio {
	
	private String nome; 
	private String codigo;
	
	public BandeiraCartaoCredito(int id, LocalDate dataCadastro, String nome, String codigo) {
		super(id, dataCadastro);
		this.nome = nome;
		this.codigo = codigo;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	} 
}
