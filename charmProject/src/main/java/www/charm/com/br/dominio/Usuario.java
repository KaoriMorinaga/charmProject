package www.charm.com.br.dominio;

import java.time.LocalDate;

public class Usuario extends EntidadeDominio {
	
	private String login; 
	private String senha; 
	private TipoUsuario tipoUsuario;
	private Status status; 
	
	public Usuario(int id, LocalDate dataCadastro, String login, String senha, TipoUsuario tipoUsuario, Status status) {
		super(id, dataCadastro);
		this.login = login;
		this.senha = senha;
		this.tipoUsuario = tipoUsuario;
		this.status = status; 
	}
	
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public TipoUsuario getTipoUsuario() {
		return tipoUsuario;
	}
	public void setTipoUsuario(TipoUsuario tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}	
}	
