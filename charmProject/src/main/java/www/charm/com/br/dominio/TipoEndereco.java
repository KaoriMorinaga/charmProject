package www.charm.com.br.dominio;

import java.time.LocalDate;

public class TipoEndereco extends EntidadeDominio{
	
	private String nome; 
	private String descricao;
	
	public TipoEndereco(int id, LocalDate dataCadastro, String nome, String descricao) {
		super(id, dataCadastro);
		this.nome = nome;
		this.descricao = descricao;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}
