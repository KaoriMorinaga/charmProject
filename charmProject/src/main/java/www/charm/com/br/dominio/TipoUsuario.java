package www.charm.com.br.dominio;

import java.time.LocalDate;

public class TipoUsuario extends EntidadeDominio {

	private String nome; 
	private String descricao; 
	private Permissao permissao;
	
	public TipoUsuario(int id, LocalDate dataCadastro, String nome, String descricao, Permissao permissao) {
		super(id, dataCadastro);
		this.nome = nome;
		this.descricao = descricao;
		this.permissao = permissao;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Permissao getPermissao() {
		return permissao;
	}
	public void setPermissao(Permissao permissao) {
		this.permissao = permissao;
	}
}
