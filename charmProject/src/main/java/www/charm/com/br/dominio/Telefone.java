package www.charm.com.br.dominio;

import java.time.LocalDate;

public class Telefone extends EntidadeDominio {
	
	private String ddd; 
	private String numero; 
	private String tipo;
	
	public Telefone(int id, LocalDate dataCadastro, String ddd, String numero, String tipo) {
		super(id, dataCadastro);
		this.ddd = ddd;
		this.numero = numero;
		this.tipo = tipo;
	}
	
	public String getDdd() {
		return ddd;
	}
	public void setDdd(String ddd) {
		this.ddd = ddd;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	} 
}
