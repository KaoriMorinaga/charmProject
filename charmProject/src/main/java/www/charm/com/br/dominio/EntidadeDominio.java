package www.charm.com.br.dominio;

import java.time.LocalDate;

public class EntidadeDominio {
	
	private int id; 
	private LocalDate dataCadastro;
	
	public EntidadeDominio(int id, LocalDate dataCadastro) {
		super();
		this.id = id;
		this.dataCadastro = dataCadastro;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public LocalDate getDataCadastro() {
		return dataCadastro;
	}
	public void setDataCadastro(LocalDate dataCadastro) {
		this.dataCadastro = dataCadastro;
	}
}
