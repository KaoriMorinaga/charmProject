package www.charm.com.br.dominio;

import java.time.LocalDate;
import java.util.ArrayList;

public class Cliente extends EntidadeDominio {
	
	private String nome; 
	private String sobrenome; 
	private LocalDate dataNascimento; 
	private String cpf; 
	private String genero; 
	private Usuario usuario; 
	private ArrayList<Endereco> enderecos; 
	private ArrayList<CartaoCredito> cartoesCredito; 
	private ArrayList<Telefone> telefones; 
	private String ranking;
	
	public Cliente(int id, LocalDate dataCadastro, String nome, String sobrenome, LocalDate dataNascimento, String cpf,
			String genero, Usuario usuario, ArrayList<Endereco> enderecos, ArrayList<CartaoCredito> cartoesCredito,
			ArrayList<Telefone> telefones, String ranking) {
		super(id, dataCadastro);
		this.nome = nome;
		this.sobrenome = sobrenome;
		this.dataNascimento = dataNascimento;
		this.cpf = cpf;
		this.genero = genero;
		this.usuario = usuario;
		this.enderecos = enderecos;
		this.cartoesCredito = cartoesCredito;
		this.telefones = telefones;
		this.ranking = ranking;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getSobrenome() {
		return sobrenome;
	}
	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}
	public LocalDate getDataNascimento() {
		return dataNascimento;
	}
	public void setDataNascimento(LocalDate dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getGenero() {
		return genero;
	}
	public void setGenero(String genero) {
		this.genero = genero;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public ArrayList<Endereco> getEnderecos() {
		return enderecos;
	}
	public void setEnderecos(ArrayList<Endereco> enderecos) {
		this.enderecos = enderecos;
	}
	public ArrayList<CartaoCredito> getCartoesCredito() {
		return cartoesCredito;
	}
	public void setCartoesCredito(ArrayList<CartaoCredito> cartoesCredito) {
		this.cartoesCredito = cartoesCredito;
	}
	public ArrayList<Telefone> getTelefones() {
		return telefones;
	}
	public void setTelefones(ArrayList<Telefone> telefones) {
		this.telefones = telefones;
	}
	public String getRanking() {
		return ranking;
	}
	public void setRanking(String ranking) {
		this.ranking = ranking;
	} 
}
