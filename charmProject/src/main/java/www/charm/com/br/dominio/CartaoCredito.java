package www.charm.com.br.dominio;

public class CartaoCredito {
	
	private String numero; 
	private String nomeImpresso; 
	private String codigoSegurança; 
	private BandeiraCartaoCredito bandeira;
	private TipoCartaoCredito tipoCartao; 

	public CartaoCredito(String numero, String nomeImpresso, String codigoSegurança, BandeiraCartaoCredito bandeira,
			TipoCartaoCredito tipoCartao) {
		super();
		this.numero = numero;
		this.nomeImpresso = nomeImpresso;
		this.codigoSegurança = codigoSegurança;
		this.bandeira = bandeira;
		this.tipoCartao = tipoCartao;
	}
	
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getNomeImpresso() {
		return nomeImpresso;
	}
	public void setNomeImpresso(String nomeImpresso) {
		this.nomeImpresso = nomeImpresso;
	}
	public String getCodigoSegurança() {
		return codigoSegurança;
	}
	public void setCodigoSegurança(String codigoSegurança) {
		this.codigoSegurança = codigoSegurança;
	}
	public BandeiraCartaoCredito getBandeira() {
		return bandeira;
	}
	public void setBandeira(BandeiraCartaoCredito bandeira) {
		this.bandeira = bandeira;
	}
	public TipoCartaoCredito getTipoCartao() {
		return tipoCartao;
	}
	public void setTipoCartao(TipoCartaoCredito tipoCartao) {
		this.tipoCartao = tipoCartao;
	} 
}
